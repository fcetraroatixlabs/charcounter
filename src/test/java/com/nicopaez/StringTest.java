package com.nicopaez;

import static org.junit.Assert.assertNotEquals;

import java.util.HashMap;

import org.junit.Test;

public class StringTest {
	@Test
	public void checkThatCharAtPositionBeEqualToGivenChar() {
		String counter = new String("Hola mundo");
		assert (counter.charAt(2) == 'l');
	}

	@Test
	public void checkThatInputSizeEqualToGivenSize() {
		String counter = new String("Hola mundo");
		assert (counter.length() == 10);
	}

	@Test
	public void checkThatCharAtPositionBeNotEqualToGivenChar() {
		String counter = new String("Hola mundo");
		assertNotEquals(counter.charAt(2), 'f');
	}

	@Test
	public void checkThatInputSizeNotEqualToGivenSize() {
		String counter = new String("Hola mundo");
		assertNotEquals(counter.length(), 12);
	}
}
