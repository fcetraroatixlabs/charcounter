package com.nicopaez;

import org.junit.Test;

import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;

public class CharCounterTest {

	@Test
	public void whenInputIsEmptyResultIsEmpty() {
		CharCounter counter = new CharCounter();
		HashMap<Character, Integer> result = counter.countAll("");
		assert (result.isEmpty());
	}

	@Test
	public void whenInputIsNotEmptyResultIsNotEmpty() {
		CharCounter counter = new CharCounter();
		HashMap<Character, Integer> result = counter.countAll("Hola mundo!");
		assert (!result.isEmpty());
	}

	@Test
	public void whenCharToCheckIsPresentTwoTimesResultIsTwo() {
		char charToCheck = 'o';
		CharCounter counter = new CharCounter();
		HashMap<Character, Integer> result = counter.countAll("Hola mundo!");
		assert (result.get(charToCheck) == 2);
	}

	@Test
	public void whenCharToCheckIsNotPresentResultIsZero() {
		char charToCheck = 'y';
		CharCounter counter = new CharCounter();
		HashMap<Character, Integer> result = counter.countAll("Hola mundo!");
		assert (result.get(charToCheck) == 0);
	}

	// CharCounter replace all dash characters in input for underscores
	@Test
	public void whenCharToCheckIsDashResultIsZero() {
		char charToCheck = '-';
		CharCounter counter = new CharCounter();
		HashMap<Character, Integer> result = counter.countAll("Hola-mundo!");
		assert (result.get(charToCheck) == 0);
	}
}
