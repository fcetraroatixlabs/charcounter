package com.nicopaez;

import java.util.HashMap;
/*
 * Esta clase cuante la cantidad de caracteres que contenidos en un texto
 */
public class CharCounter {

    /*
     * Entrada: un string cuyos caracteres se contabilizaran
     * Salida: un hashmap (diccionario) donde la clave es un caracter y el contenido representa
     * la cantidad de apariciones del caracter en cuestión dentro del string de entrada.
     */
    public HashMap<Character, Integer> countAll(String text) {
        HashMap<Character, Integer> results = new DefaultHashMap<Character, Integer>(0);
        if (null == text || text.length() == 0) {
            return results;
        }
        text = text.toLowerCase();
        if (text.contains("-")) {
            text = text.replaceAll("-", "_");
        }
        for(int i=0; i < text.length(); i++) {
            int count = 0;
            char c = text.charAt(i);
            if(results.containsKey(c)) {
                count = results.get(c);
            }
            count++;
            results.put(c, count);
        }
        return results;
    }

    class DefaultHashMap<K,V> extends HashMap<K,V> {
        protected V defaultValue;
        public DefaultHashMap(V defaultValue) {
            this.defaultValue = defaultValue;
        }
        @Override
        public V get(Object k) {
            return containsKey(k) ? super.get(k) : defaultValue;
        }
    }
}